
<style>

    hr {
        width: 120px;
        height: 2px;
        border: solid 1px #603b80;
    }
    .loader {
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #dedde3;
        width: 60px;
        height: 60px;
        z-index: 9999;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }

</style>

@extends('layout.app')
@section('content')

    <div class="creditly-wrapper col-md-8 border mx-auto bg-white align-middle ">

        <div class="row">
            <div class="w-100">
                <div class="mx-auto w-100 p-3 text-dark text-center merchantName" style="background-color: #dedde3;">
                    <span> Yaya Towers </span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="mx-auto w-100 text-center text-secondary pt-4 merchantTitle">
                    <span>Enter your details to complete your payment below</span>
                </div>
                <hr class="col-2 mx-auto"/>
            </div>
        </div>

        <div id="error_div" class="alert alert-danger col-8 mx-auto justify-content-center"></div>
        {{--<div id="loading_gif" class="row mx-auto justify-content-center">
            <div class="loader"></div>
        </div>--}}

        <form class="form-group" name="deform" id="deform" method="post" autocomplete="off">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group row">
                <label for="amount" class="col-sm-3 col-form-label">Amount</label>
                <div class="col-sm-9">
                    <input type="number" id='pay_amount' class = 'form-control' name = 'pay_amount' step="0.001" placeholder = 'Enter amount' >
                </div>
            </div>
            <div class="form-group row">
                <label for="reference" class="col-sm-3 col-form-label">Reference Number</label>
                <div class="col-sm-9">
                    <input type="text" id='pay_reference' class = 'form-control' name = 'pay_reference' placeholder = 'Enter reference number' >
                </div>
            </div>

            <div class="row pb-1 pt-1">
                <input id="submit" type="submit" class="btn btn-light btn-circle col-4 mx-auto bg-white border" value="Proceed" />
            </div>
        </form>
    </div>

    <form id="eazzycheckout-payment-form" action="https://api.equitybankgroup.com/v2/checkout/launch" method="POST">
        <input type="hidden" id="token" name="token">
        <input type="hidden" id="amount" name="amount">
        <input type="hidden" id="currency" name="currency" value="KES">
        <input type="hidden" id="orderReference" name="orderReference">
        <input type="hidden" id="popupLogo" name="popupLogo" value="https://www.yaya-apartments.com/wp-content/uploads/YayaTowersHotelLogo-e1449728599979.png">
        <input type="hidden" id="merchantCode" name="merchantCode" value="1291888195">
        <input type="hidden" id="outletCode" name="outletCode" value="0000000000">
        <input type="hidden" id="merchant" name="merchant" value="Yaya Towers">
        <input type="hidden" id="expiry" name="expiry" value="2025-02-17T19:00:00">
        <input type="hidden" id="custName" name="custName" value="Test Customer">
        <input type="hidden" id="ez1_callbackurl" name="ez1_callbackurl" value="https://www.yaya-apartments.com/">
        <input type="hidden" id="ez2_callbackurl" name="ez2_callbackurl" value="https://www.yaya-apartments.com/">
        <div class="row">
            <div class="col-md-6 text-center mx-auto pt-2">
                <input type="submit" hidden id="submit-cg" role="button" class="btn btn-primary col-md-3" value="Checkout"/>
            </div>
        </div>
    </form>

@endsection