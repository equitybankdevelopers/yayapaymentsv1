<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
URL::forceScheme("https");

Route::get('/', 'PayController@index');

Route::get('/processPayment', function () {
    return view('processPayment');
});

/*Get token: POST*/
Route::post('/processToken', 'PayController@getToken');

/* Get merchant Details : POST*/
Route::post('/getMerchant', 'PayController@getMerchantDetails');

/* Create Bill : POST*/
Route::post('/payment', 'PayController@launch');
