$( document ).ready(function(){init();$("#error_div").hide();$("#loading_gif").hide();}) ;

function init(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'post',
        url: '/processToken',
        data: {
            _token: CSRF_TOKEN,
        },
        success: function success(response){
            if (response !== null || response !== ''){
                $('#token').val(response);
               // console.log(response);
            } else {
               // console.log(response);
            }
        },
        error: function error(e){
            error('System Error, please try again!');
            init();
        }
    });
}

$('#deform').on('submit', function(e){
    e.preventDefault();

    let amount = $('#pay_amount').val();
    let reference = $('#pay_reference').val();

    if (amount.length > 0 && reference.length > 0) {
        $("#amount").val(amount);
        $("#orderReference").val(reference);
        $( "#submit-cg" ).trigger( "click" );
    } else {
        error('Enter Required Field');
        console.log('Fill required fields');
    }
});

function error(m) {
    var x = $("#error_div");
    x.show(800);
    x.text(m);
    setTimeout(function(){ x.hide(1000); }, 4000);
    console.log('Handle error');
}

function loader_start() {
    var x = $("#loading_gif");
    x.show(800);
    $(':input[type="submit"]').prop('disabled', true);
}

function loader_end() {
    var x = $("#loading_gif");
    x.hide(800);
    $(':input[type="submit"]').prop('disabled', false);
}