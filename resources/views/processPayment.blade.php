{{--
/**
 * Created by PhpStorm.
 * User: kevinknight
 * Date: 8/26/18
 * Time: 1:42 AM
 */--}}
<style>

  hr {
    width: 120px;
    height: 2px;
    border: solid 1px #603b80;
  }

</style>
@extends('layout.app')
@section('content')

  <div class="col-md-8 border mx-auto bg-white align-middle ">

    <div class="row">
      <div class="w-100">
        <div class="mx-auto w-100 p-3 text-dark text-center merchantName" style="background-color: #dedde3;">
          <span>Yaya Towers</span>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col">
        <div class="mx-auto w-100 p-3 text-center text-secondary pt-5 merchantTitle">
          <span>Your payment was successful!</span>
        </div>
        <hr class="col-2"/>
      </div>
    </div>

    <div class="row pt-5">
      <div class="col">
        <img class="rounded mx-auto d-block" src="public/img/success.png">
      </div>
    </div>

    <div class="row" style="padding-top: 50px;padding-bottom: 150px;">
      <div class="col">
        <p class="text-center Transaction-reference">Transaction reference: <span class="font-weight-bold">{{session('responseData')['transactionId']}}</span></p>
      </div>
    </div>

      <div class="row pb-5">
        <a id="submit" class="btn btn-light btn-circle col-4 mx-auto bg-white border" href="{{ url('/') }}" role="button">Make another payment</a>
      </div>

  </div>





{{--<div class="jumbotron">
  <div class=" text-center">
    <p class="display-4 text-primary mb-3">Payment {{session('responseData')['status']}}</p>
    <p class="lead">Transaction Reference: <span class="font-weight-bold">{{session('responseData')['transactionId']}}</span></p>
  </div>
  <hr class="my-4">
  <a class="btn btn-primary btn-lg align-items-start" href="{{ url('/') }}" role="button">Back to Payments</a>
</div>--}}
@endsection
