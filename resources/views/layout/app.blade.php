{{--
/**
 * Created by PhpStorm.
 * User: kevinknight
 * Date: 8/26/18
 * Time: 1:44 AM
 */--}}

<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Finserve">
    <!-- CSRF_token -->
    <meta name="csrf-token" content="{{csrf_token()}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- project css -->
    <link rel="stylesheet" href="{{ secure_asset('public/css/app.css') }}">
    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/js/bootstrap-formhelpers.min.js"></script>
    <title>Yaya Towers</title>
    <style>
        .Powered-by-Jenga-PGW {
            font-family: Nunito;
            font-size: 16px;
            font-weight: normal;
            font-style: normal;
            letter-spacing: normal;
            color: #f9f9f9;
        }
    </style>
</head>
<body>

<div class="container pt-5" style="margin-top: 50px;">
    <div class="align-content-center">
        @include('inc.messages')
        @yield('content')
    </div>
</div>

<footer class="footer col-md-8 pt-5 pb-2 mx-auto align-middle Powered-by-Jenga-PGW ">
    <div class="text-center pb-3">
        <img class="rounded mx-auto d-block" src="{{ asset('public/img/jenga-logo.svg') }}">
    </div>
    <br/>
    <div class="text-center text-white pb-3">
        <p>Powered by
            <a class="text-white mr-2 col-xs-12" href="https://jengapgw.io/" target="_blank">Jenga PGW  </a>
             •  Need a faster payment experience?
            <a class="text-white mr-2 col-xs-12 " href="https://jengahq.typeform.com/to/acVeS3" target="_blank"> Sign up </a>  •
            <a class="text-white col-xs-12 " href="https://finserve.africa/privacy" target="_blank">  Privacy Policy</a>
        </p>
    </div>
</footer>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="{{ secure_asset('public/js/app.js') }}"></script>
<script src="{{ secure_asset('public/js/main.js') }}"></script>

</body>
</html>