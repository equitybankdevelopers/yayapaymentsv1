<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
//use App\Message;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Middleware;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
//use Illuminate\Support\Facades\Redirect;

class PayController extends Controller
{
    public function index(){
        return view('welcome');
    }

    public function getToken(){

        $key ='akxUV3pubjZ2aTNkRzlxVG9PVzBJZ0s5OXpaVVNaM0U6NG14b1kwVkRjcVA4UW92Yw==';

        $options = http_build_query(array(
            'username'=>'1291888195',
            'password'=>'Xq8q6CAUo8iYv6UsbtmGkW489mj9siFr',
            'grant_type'=>'password',
            'merchant'=>'Apigee001',
            'service'=> 'ECOMMERCE'
        ));

        $url = 'https://api.equitybankgroup.com/identity/v1/merchant-token';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $options);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Basic ' .$key
        ));
        $res = curl_exec($ch);
        curl_close($ch);

        $tokens = json_decode($res, true);

        $payToken = $tokens['access_token'];
        Session::put('token', $payToken);

        return $payToken;
    }
}